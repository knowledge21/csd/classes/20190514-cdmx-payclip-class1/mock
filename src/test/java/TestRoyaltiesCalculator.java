import com.payclip.k21.CommissionCalculator;
import com.payclip.k21.royaltyCalculator;
import com.payclip.k21.SalesRepository;
import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;

import static org.mockito.Mockito.*;

public class TestRoyaltiesCalculator {

    SalesRepository salesRepository =  mock(SalesRepository.class);
    CommissionCalculator commissionCalculator = mock(CommissionCalculator.class);

    royaltyCalculator royaltieCalculator=new royaltyCalculator(salesRepository, commissionCalculator);

    @Test
    public void testNoSalesReturns0Royalties() {
        int year = 2020;
        int month = 12;
        double expectedRoyalties = 0;

        when(salesRepository.getSales(year, month)).thenReturn(Arrays.asList());
        double result = royaltieCalculator.calculateRoyalties(year, month);

        Assert.assertEquals(expectedRoyalties, result, 0);


    }

    @Test
    public void testSalesfor5CommissionReturnsRoyalties() {
        int year = 2018;
        int month = 12;
        double expectedRoyalties = 304;

        when(salesRepository.getSales(year, month)).thenReturn(Arrays.asList(1600));

        double result = royaltieCalculator.calculateRoyalties(year, month);

        Assert.assertEquals(expectedRoyalties, result, 0);

    }

    @Test
    public void testSalesfor6CommissionReturnsRoyalties() {
        int year = 2018;
        int month = 11;
        double expectedRoyalties = 1880.18;

        when(salesRepository.getSales(year, month)).thenReturn(Arrays.asList(10001));

        double result = royaltieCalculator.calculateRoyalties(year, month);

        Assert.assertEquals(expectedRoyalties, result, 0);


    }


    @Test
    public void testRoyaltiesforMultipleSales() {
        int year = 2018;
        int month = 10;
        double expectedRoyalties = 6056.0;

        when(salesRepository.getSales(year, month)).thenReturn(Arrays.asList(10000,10000,12000));

        double result = royaltieCalculator.calculateRoyalties(year, month);

        Assert.assertEquals(expectedRoyalties, result, 0);


    }

    @Test
    public void testCalculateSingleRoyaltyWith10001Returns1880_18() {
        Integer sale = 10001;
        double expectedResult = 1880.18;

        double result = royaltieCalculator.calculateRoyalty(sale);

        Assert.assertEquals(expectedResult, result, 0);
    }
}








