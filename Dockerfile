FROM openjdk:8-alpine
COPY build/libs/calculator-0.1.0.jar /calculator-0.1.0.jar

EXPOSE 8080

CMD java -jar /calculator-0.1.0.jar